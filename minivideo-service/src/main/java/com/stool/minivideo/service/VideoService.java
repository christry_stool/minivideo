package com.stool.minivideo.service;

import com.stool.minivideo.pojo.Comment;
import com.stool.minivideo.pojo.Video;
import com.stool.minivideo.utils.PagedResult;

import java.util.List;

public interface VideoService {

    /**
     * @Description: 保存视频
     * @param video
     */
    public String saveVideo(Video video);

    /**
     * @Description: 修改视频的封面
     * @param videoId
     * @param coverPath
     */
    public void updateVideo(String videoId, String coverPath);

    /**
     * @Description: 分页查询视频列表
     * @param page
     * @param pageSize
     * @return
     */
    public PagedResult getAllVideos(Video video, Integer isSaveRecord, Integer page, Integer pageSize);

    /**
     * 查询我喜欢的视频列表
     * @param userId
     * @param page
     * @param pageSize
     * @return
     */
    public PagedResult queryMyLikeVideos(String userId, Integer page, Integer pageSize);

    /**
     * 查询我关注的人的视频列表
     * @param userId
     * @param page
     * @param pageSize
     * @return
     */
    public PagedResult queryMyFollowVideos(String userId, Integer page, Integer pageSize);

    /**
     * @Description: 获取热搜词列表
     * @return
     */
    public List<String> getHotwords();


    /**
     * @Description: 用户喜欢视频
     * @param userId
     * @param videoId
     * @param videoCreaterId
     */
    public void userLikeVideo(String userId, String videoId, String videoCreaterId);

    /**
     * Description: 用户不喜欢视频
     * @param userId
     * @param videoId
     * @param videoCreaterId
     */
    public void userUnlikeVideo(String userId, String videoId, String videoCreaterId);

    /**
     * 用户留言
     * @param comment
     */
    public void saveComment(Comment comment);

    /**
     * 留言分页
     * @param videoId
     * @param page
     * @param pageSize
     * @return
     */
    public PagedResult getAllComments(String videoId, Integer page, Integer pageSize);
}