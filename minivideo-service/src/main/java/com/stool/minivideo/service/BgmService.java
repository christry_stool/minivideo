package com.stool.minivideo.service;

import com.stool.minivideo.pojo.Bgm;

import java.util.List;

public interface BgmService {

    /**
     * @Description: 查询背景音乐列表
     * @return
     */
    public List<Bgm> queryBgmList();

    /**
     * @DEscription: 根据id查询bgm
     * @param bgmId
     * @return
     */
    public Bgm queryBgmById(String bgmId);
}
