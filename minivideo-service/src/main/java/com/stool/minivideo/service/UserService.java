package com.stool.minivideo.service;

import com.stool.minivideo.pojo.User;
import com.stool.minivideo.pojo.UserReport;

public interface UserService {

    /**
     * @Description: 判断用户名是否存在
     * @param username
     * @return
     */
    public boolean queryUsernameIsExist(String username);

    /**
     * @Decription: 保存用户（用于注册）
     * @param user
     */
    public void saveUser(User user);

    /**
     * @Decription: 用户登录，根据用户名和密码查询用户
     * @param username
     * @param password
     * @return
     */
    public User queryUserForLogin(String username, String password);

    /**
     * @Description: 用户修改信息
     * @param user
     */
    public void updateUserInfo(User user);

    /**
     * @Description: 查询用户信息
     * @param userId
     * @return
     */
    public User queryUserInfo(String userId);

    /**
     * @Description: 查询用户是否喜欢视频
     * @param userId
     * @param videoId
     * @return
     */
    public boolean isUserLikeVideo(String userId, String videoId);

    /**
     * 增加用户与粉丝的关系
     * @param userId
     * @param fanId
     */
    public void saveUserFanRelation(String userId, String fanId);

    /**
     * 删除用户与粉丝的关系
     * @param userId
     * @param fanId
     */
    public void deleteUserFanRelation(String userId, String fanId);

    /**
     * 查询用户是否关注
     * @param userId
     * @param fanId
     * @return
     */
    public boolean queryIsFollow(String userId, String fanId);

    /**
     * 举报用户
     * @param userReport
     */
    public void reportUser(UserReport userReport);
}
