package com.stool.minivideo.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.stool.minivideo.mapper.*;
import com.stool.minivideo.pojo.Comment;
import com.stool.minivideo.pojo.SearchRecord;
import com.stool.minivideo.pojo.UserLikeVideo;
import com.stool.minivideo.pojo.Video;
import com.stool.minivideo.pojo.vo.CommentVO;
import com.stool.minivideo.pojo.vo.VideoVO;
import com.stool.minivideo.service.VideoService;
import com.stool.minivideo.utils.PagedResult;
import com.stool.minivideo.utils.TimeAgoUtils;
import org.n3r.idworker.Sid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.Date;
import java.util.List;

@Service
public class VideoServiceImpl implements VideoService {

    @Autowired
    private VideoMapper videoMapper;

    @Autowired
    private VideoMapperCustom videoMapperCustom;

    @Autowired
    private SearchRecordMapper searchRecordMapper;

    @Autowired
    private UserLikeVideoMapper userLikeVideoMapper;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private CommentMapper commentMapper;

    @Autowired
    private CommentMapperCustom commentMapperCustom;

    @Autowired
    private Sid sid;

    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public String saveVideo(Video video) {
        String id = sid.nextShort();
        video.setId(id);
        videoMapper.insertSelective(video);
        return id;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public void updateVideo(String videoId, String coverPath) {
        Video video = new Video();
        video.setId(videoId);
        video.setCoverPath(coverPath);
        videoMapper.updateByPrimaryKeySelective(video);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public PagedResult getAllVideos(Video video, Integer isSaveRecord, Integer page, Integer pageSize) {

        // 保存热搜词
        String desc = video.getVideoDesc();
        String userId = video.getUserId();
        if (isSaveRecord != null && isSaveRecord == 1) {
            SearchRecord record = new SearchRecord();
            String recordId = sid.nextShort();
            record.setId(recordId);
            record.setContent(desc);
            searchRecordMapper.insert(record);
        }

        PageHelper.startPage(page, pageSize);
        List<VideoVO> list = videoMapperCustom.queryAllVideos(desc, userId);

        PageInfo<VideoVO> pageList = new PageInfo<>(list);

        PagedResult pagedResult = new PagedResult();
        pagedResult.setPage(page);
        pagedResult.setTotal(pageList.getPages());
        pagedResult.setRows(list);
        pagedResult.setRecords(pageList.getTotal());

        return pagedResult;

    }

    @Transactional(propagation = Propagation.SUPPORTS)
    @Override
    public List<String> getHotwords() {
        return searchRecordMapper.getHotwords();
    }

    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public void userLikeVideo(String userId, String videoId, String videoCreaterId) {
        // 1. 保存用户和视频的喜欢关系
        String likeId = sid.nextShort();
        UserLikeVideo ulv = new UserLikeVideo();
        ulv.setId(likeId);
        ulv.setUserId(userId);
        ulv.setVideioId(videoId);
        userLikeVideoMapper.insert(ulv);

        // 2. 视频喜欢数量累加
        videoMapperCustom.addVideoLikeCount(videoId);

        // 3. 用户受喜欢数量累加
        userMapper.addReceiveLikeCount(videoCreaterId);
    }


    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public void userUnlikeVideo(String userId, String videoId, String videoCreaterId) {
        // 1. 删除用户和视频的喜欢关系
        Example example = new Example(UserLikeVideo.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("userId", userId)
                .andEqualTo("videioId", videoId);

        userLikeVideoMapper.deleteByExample(example);

        // 2. 视频喜欢数量累减
        videoMapperCustom.reduceVideoLikeCount(videoId);

        // 3. 用户受喜欢数量累减
        userMapper.reduceReceiveLikeCount(videoCreaterId);
    }

    @Transactional(propagation = Propagation.SUPPORTS)
    @Override
    public PagedResult queryMyLikeVideos(String userId, Integer page, Integer pageSize) {
        PageHelper.startPage(page, pageSize);
        List<VideoVO> list = videoMapperCustom.queryMyLikeVideos(userId);

        PageInfo<VideoVO> pageList = new PageInfo<>(list);

        PagedResult pagedResult = new PagedResult();
        pagedResult.setPage(page);
        pagedResult.setTotal(pageList.getPages());
        pagedResult.setRows(list);
        pagedResult.setRecords(pageList.getTotal());

        return pagedResult;
    }

    @Transactional(propagation = Propagation.SUPPORTS)
    @Override
    public PagedResult queryMyFollowVideos(String userId, Integer page, Integer pageSize) {
        PageHelper.startPage(page, pageSize);
        List<VideoVO> list = videoMapperCustom.queryMyFollowVideos(userId);

        PageInfo<VideoVO> pageList = new PageInfo<>(list);

        PagedResult pagedResult = new PagedResult();
        pagedResult.setPage(page);
        pagedResult.setTotal(pageList.getPages());
        pagedResult.setRows(list);
        pagedResult.setRecords(pageList.getTotal());

        return pagedResult;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public void saveComment(Comment comment) {
        String commentId = sid.nextShort();
        comment.setId(commentId);
        comment.setCreateTime(new Date());
        commentMapper.insert(comment);
    }

    @Transactional(propagation = Propagation.SUPPORTS)
    @Override
    public PagedResult getAllComments(String videoId, Integer page, Integer pageSize) {
        PageHelper.startPage(page, pageSize);

        List<CommentVO> list = commentMapperCustom.queryComments(videoId);

        for (CommentVO c : list) {
            String timeAgo = TimeAgoUtils.format(c.getCreateTime());
            c.setTimeAgoStr(timeAgo);
        }

        PageInfo<CommentVO> pageList = new PageInfo<>(list);

        PagedResult grid = new PagedResult();
        grid.setTotal(pageList.getPages());
        grid.setRows(list);
        grid.setPage(page);
        grid.setRecords(pageList.getTotal());

        return grid;
    }
}
