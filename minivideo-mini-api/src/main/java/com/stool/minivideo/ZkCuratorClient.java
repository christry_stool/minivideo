package com.stool.minivideo;

import com.stool.minivideo.config.ResourceConfig;
import com.stool.minivideo.enums.BGMOperatorTypeEnum;
import com.stool.minivideo.pojo.Bgm;
import com.stool.minivideo.service.BgmService;
import com.stool.minivideo.utils.JsonUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.curator.RetryPolicy;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.recipes.cache.PathChildrenCache;
import org.apache.curator.framework.recipes.cache.PathChildrenCacheEvent;
import org.apache.curator.framework.recipes.cache.PathChildrenCacheListener;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Map;

public class ZkCuratorClient {

    // zk客户端
    private CuratorFramework client = null;
    private final static Logger log = LoggerFactory.getLogger(ZkCuratorClient.class);

    @Autowired
    private BgmService bgmService;

    @Autowired
    private ResourceConfig resourceConfig;

//    public static final String ZOOKEEPER_SERVER = "120.79.20.255:2181";

    public void init() {
        if (client != null) {
            return ;
        }
        // 重试策略
        RetryPolicy retryPolicy = new ExponentialBackoffRetry(1000, 5);
        // 创建zk客户端
        client = CuratorFrameworkFactory.builder()
                .connectString(resourceConfig.getZookeeperServer())
                .sessionTimeoutMs(10000)
                .retryPolicy(retryPolicy)
                .namespace("admin")
                .build();

        // 启动客户端
        client.start();

        try {
//            String testNodeData = new String(client.getData().forPath("/bgm/180607DAC4ZRZWP3"));
//            log.info("测试的节点数据为：{}", testNodeData);
            addChildWatch("/bgm");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void addChildWatch(String nodePath) throws Exception{

        final PathChildrenCache cache = new PathChildrenCache(client, nodePath, true);
        cache.start();

        cache.getListenable().addListener(new PathChildrenCacheListener() {
            @Override
            public void childEvent(CuratorFramework client, PathChildrenCacheEvent event) throws Exception {

                if (event.getType().equals(PathChildrenCacheEvent.Type.CHILD_ADDED)) {
                    log.info("监听到事件CHILD_ADDED");

                    // 1. event得到map，再从map得到type和bgmPath
                    String path = event.getData().getPath();
                    String opearatorStr = new String(event.getData().getData());

                    Map<String, String> map = JsonUtils.jsonToPojo(opearatorStr, Map.class);

                    String operatorType = map.get("operType");
                    String bgmPath = map.get("path");

                    // 2. 定义保存到本地的bgm路径
//                    String filePath = "D:/minivideo_dev" + bgmPath;
                    String filePath = resourceConfig.getFileSpace() + bgmPath;

                    // 3. 定义下载的路径（播放url）
                    String[] arrPath = bgmPath.split("\\\\");
                    String finalPath = "";
                    // 3.1 处理url的斜杠以及编码
                    for (int i = 0; i < arrPath.length; i++) {
                        if (StringUtils.isNotBlank(arrPath[i])) {
                            finalPath += "/";
                            finalPath += URLEncoder.encode(arrPath[i], "UTF-8");
                        }
                    }
//                    String bgmUrl = "http://127.0.0.1:8080/mvc" + finalPath;
                    String bgmUrl = resourceConfig.getBgmServer() + finalPath;

                    if (operatorType.equals(BGMOperatorTypeEnum.ADD.type)) {
                        // 下载bgm到springboot服务器
                        URL url = new URL(bgmUrl);
                        File file = new File(filePath);
                        FileUtils.copyURLToFile(url, file);
                        client.delete().forPath(path);
                    } else if (operatorType.equals(BGMOperatorTypeEnum.DELETE.type)) {
                        File file = new File(filePath);
                        FileUtils.forceDelete(file);
                        client.delete().forPath(path);
                    }
                }
            }
        });
    }
}
