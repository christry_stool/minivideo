package com.stool.minivideo.controller;

import com.stool.minivideo.config.ResourceConfig;
import com.stool.minivideo.pojo.User;
import com.stool.minivideo.pojo.UserReport;
import com.stool.minivideo.pojo.vo.PublisherVideo;
import com.stool.minivideo.pojo.vo.UserVO;
import com.stool.minivideo.service.UserService;
import com.stool.minivideo.utils.MinivideoJSONResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

@RestController
@Api(value = "用户相关业务的接口", tags = {"用户相关业务的controller"})
@RequestMapping("/user")
public class UserController extends BasicController{

    @Autowired
    private UserService userService;

    @Autowired
    private ResourceConfig config;

    @ApiOperation(value = "用户上传头像", notes = "用户上传头像的接口")
    @ApiImplicitParam(name = "userId", value = "用户id", required = true, dataType = "String")
    @PostMapping("/uploadFace")
    public MinivideoJSONResult uploadFace(String userId,
                                          @RequestParam("file") MultipartFile[] files) throws Exception {

        if (StringUtils.isBlank(userId)) {
            return MinivideoJSONResult.errorMsg("用户id不能为空...");
        }

        // 文件保存的命令空间
        String fileSpace = config.getFileSpace();
        // 保存到数据库中的相对路径
        String uploadPathDB = "/" + userId + "/face";

        if (files.length != 0 && files[0] != null) {
            String filePath = fileSpace + uploadPathDB + "/" + files[0].getOriginalFilename();
            boolean res = uploadFile(filePath, files[0]);
            if (res) {
                uploadPathDB = uploadPathDB + "/" + files[0].getOriginalFilename();
                User user = new User();
                user.setId(userId);
                user.setFaceImage(uploadPathDB);
                userService.updateUserInfo(user);
                return MinivideoJSONResult.ok(uploadPathDB);
            } else {
                return MinivideoJSONResult.errorMsg("上传出错...");
            }
        } else {
            return MinivideoJSONResult.errorMsg("上传出错...");
        }

    }

    @ApiOperation(value = "查询用户信息", notes = "查询用户信息的接口")
    @ApiImplicitParam(name = "userId", value = "用户id", required = true, dataType = "String", paramType = "query")
    @PostMapping("/query")
    public MinivideoJSONResult query(String userId, String fanId) throws Exception {
        if (StringUtils.isBlank(userId)) {
            return MinivideoJSONResult.errorMsg("用户id不能为空...");
        }

        User userInfo = userService.queryUserInfo(userId);
        UserVO userVO = new UserVO();
        BeanUtils.copyProperties(userInfo, userVO);

        boolean isFollow = userService.queryIsFollow(userId, fanId);
        userVO.setFollow(isFollow);

        return MinivideoJSONResult.ok(userVO);
    }

    @PostMapping("/queryPublisher")
    public MinivideoJSONResult queryPublisher(String loginUserId, String videoId, String publishUserId) throws Exception {

        if (StringUtils.isBlank(publishUserId)) {
            return MinivideoJSONResult.errorMsg("");
        }

        // 1. 查询视频发布者的信息
        User userInfo = userService.queryUserInfo(publishUserId);
        UserVO publisher = new UserVO();
        BeanUtils.copyProperties(userInfo, publisher);

        // 2. 查询当前登录者和视频的点赞关系
        boolean userLikeVideo = userService.isUserLikeVideo(loginUserId, videoId);
        PublisherVideo publisherVideo = new PublisherVideo();
        publisherVideo.setPublisher(publisher);
        publisherVideo.setUserLikeVideo(userLikeVideo);

        return MinivideoJSONResult.ok(publisherVideo);
    }

    @PostMapping("/beYourFans")
    public MinivideoJSONResult beYourFans(String userId, String fanId) throws Exception {
        if (StringUtils.isBlank(userId) || StringUtils.isBlank(fanId)) {
            return MinivideoJSONResult.errorMsg("");
        }

        userService.saveUserFanRelation(userId, fanId);

        return MinivideoJSONResult.ok("关注成功...");
    }

    @PostMapping("/dontBeYourFans")
    public MinivideoJSONResult dontBeYourFans(String userId, String fanId) throws Exception {
        if (StringUtils.isBlank(userId) || StringUtils.isBlank(fanId)) {
            return MinivideoJSONResult.errorMsg("");
        }

        userService.deleteUserFanRelation(userId, fanId);

        return MinivideoJSONResult.ok("取消关注成功...");
    }

    @PostMapping("/reportUser")
    public MinivideoJSONResult reportUser(@RequestBody UserReport userReport) throws Exception {
        // 保存举报信息
        userService.reportUser(userReport);

        return MinivideoJSONResult.errorMsg("举报成功...");
    }


}
