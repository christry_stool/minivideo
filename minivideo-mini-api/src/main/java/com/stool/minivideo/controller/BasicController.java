package com.stool.minivideo.controller;

import com.stool.minivideo.config.ResourceConfig;
import com.stool.minivideo.utils.MinivideoJSONResult;
import com.stool.minivideo.utils.RedisOperator;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

@RestController
public class BasicController {

    @Autowired
    public RedisOperator redis;

    public static final String USER_REDIS_SESSION = "user-redis-session";

    // 分页查询时的每页条数
    public static final Integer PAGE_SIZE = 5;

    public boolean uploadFile(String filePath, MultipartFile file) {

        if (file == null) {
            return false;
        }
        FileOutputStream fileOutputStream = null;
        InputStream inputStream = null;
        try {

            File outFile = new File(filePath);
            if (outFile.getParentFile() != null && !outFile.getParentFile().isDirectory()) {
                // 创建父文件夹
                boolean r = outFile.getParentFile().mkdirs();
            }

            fileOutputStream = new FileOutputStream(outFile);
            inputStream = file.getInputStream();
            IOUtils.copy(inputStream, fileOutputStream);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.flush();
                    fileOutputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return true;
    }


}
