package com.stool.minivideo;

import com.stool.minivideo.config.ResourceConfig;
import com.stool.minivideo.controller.interceptor.MiniInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

    @Autowired
    private ResourceConfig resourceConfig;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/**")
                .addResourceLocations("classpath:/META-INF/resources/")
                .addResourceLocations("file:" + resourceConfig.getFileSpace() + "/");
    }

    @Bean(initMethod = "init")
    public ZkCuratorClient zkCuratorClient() {
        return new ZkCuratorClient();
    }

    @Bean
    public MiniInterceptor miniInterceptor() {
        return new MiniInterceptor();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(miniInterceptor())
                .addPathPatterns("/user/**")
                .addPathPatterns("/bgm/list")
                .addPathPatterns("/video/upload", "/video/uploadCover", "/video/userLike", "/video/userUnlike")
                .excludePathPatterns("/user/queryPublisher");

    }
}
