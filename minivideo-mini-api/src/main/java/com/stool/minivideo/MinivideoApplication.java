package com.stool.minivideo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import tk.mybatis.spring.annotation.MapperScan;

@SpringBootApplication
@MapperScan(basePackages="com.stool.minivideo.mapper")
@ComponentScan(basePackages= {"com.stool.minivideo", "org.n3r.idworker"})
public class MinivideoApplication {
    public static void main(String[] args) {
        SpringApplication.run(MinivideoApplication.class, args);
    }
}
