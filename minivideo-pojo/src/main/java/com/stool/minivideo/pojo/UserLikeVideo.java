package com.stool.minivideo.pojo;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "user_like_video")
public class UserLikeVideo {
    @Id
    private String id;

    @Column(name = "user_id")
    private String userId;

    @Column(name = "videio_id")
    private String videioId;

    /**
     * @return id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return user_id
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * @return videio_id
     */
    public String getVideioId() {
        return videioId;
    }

    /**
     * @param videioId
     */
    public void setVideioId(String videioId) {
        this.videioId = videioId;
    }
}