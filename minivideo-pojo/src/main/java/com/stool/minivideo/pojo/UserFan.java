package com.stool.minivideo.pojo;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "user_fan")
public class UserFan {
    @Id
    private String id;

    @Column(name = "user_id")
    private String userId;

    @Column(name = "fan_id")
    private String fanId;

    /**
     * @return id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return user_id
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * @return fan_id
     */
    public String getFanId() {
        return fanId;
    }

    /**
     * @param fanId
     */
    public void setFanId(String fanId) {
        this.fanId = fanId;
    }
}