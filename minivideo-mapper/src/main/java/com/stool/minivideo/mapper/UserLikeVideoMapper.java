package com.stool.minivideo.mapper;

import com.stool.minivideo.pojo.UserLikeVideo;
import com.stool.minivideo.utils.MyMapper;

public interface UserLikeVideoMapper extends MyMapper<UserLikeVideo> {
}