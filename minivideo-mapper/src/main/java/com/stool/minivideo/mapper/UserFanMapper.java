package com.stool.minivideo.mapper;

import com.stool.minivideo.pojo.UserFan;
import com.stool.minivideo.utils.MyMapper;

public interface UserFanMapper extends MyMapper<UserFan> {
}