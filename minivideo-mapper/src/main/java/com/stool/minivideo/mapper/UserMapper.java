package com.stool.minivideo.mapper;

import com.stool.minivideo.pojo.User;
import com.stool.minivideo.utils.MyMapper;

public interface UserMapper extends MyMapper<User> {

    /**
     * @Description: 用户受喜欢数累加
     * @param userId
     */
    public void addReceiveLikeCount(String userId);

    /**
     * @Description: 用户受喜欢数累减
     * @param userId
     */
    public void reduceReceiveLikeCount(String userId);

    /**
     * @Description: 增加粉丝数量
     * @param userId
     */
    public void addFansCount(String userId);

    /**
     * @Description: 减少粉丝数
     * @param userId
     */
    public void reduceFansCount(String userId);

    /**
     * 增加关注数
     * @param userId
     */
    public void addFollersCount(String userId);

    /**
     * 减少关注数
     * @param userId
     */
    public void reduceFollersCount(String userId);
}