package com.stool.minivideo.mapper;

import com.stool.minivideo.pojo.UserReport;
import com.stool.minivideo.utils.MyMapper;

public interface UserReportMapper extends MyMapper<UserReport> {
}