package com.stool.minivideo.mapper;

import com.stool.minivideo.pojo.Video;
import com.stool.minivideo.pojo.vo.VideoVO;
import com.stool.minivideo.utils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface VideoMapperCustom extends MyMapper<Video> {

    /**
     * 条件查询所有视频列表
     * @param videoDesc
     * @param userId
     * @return
     */
    public List<VideoVO> queryAllVideos(@Param("videoDesc") String videoDesc, @Param("userId") String userId);

    /**
     * 查询关注的视频
     * @param userId
     * @return
     */
    public List<VideoVO> queryMyFollowVideos(String userId);

    /**
     * 查询点赞视频
     * @param userId
     * @return
     */
    public List<VideoVO> queryMyLikeVideos(String userId);

    /**
     * @Description: 对视频喜欢的数量进行累加
     * @param videoId
     */
    public void addVideoLikeCount(String videoId);

    /**
     * @Description: 对视频喜欢的数量进行累减
     * @param videoId
     */
    public void reduceVideoLikeCount(String videoId);
}