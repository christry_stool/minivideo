package com.stool.minivideo.mapper;

import com.stool.minivideo.pojo.Comment;
import com.stool.minivideo.utils.MyMapper;

public interface CommentMapper extends MyMapper<Comment> {
}