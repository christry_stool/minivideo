package com.stool.minivideo.mapper;

import com.stool.minivideo.pojo.Bgm;
import com.stool.minivideo.utils.MyMapper;

public interface BgmMapper extends MyMapper<Bgm> {
}