package com.stool.minivideo.mapper;

import com.stool.minivideo.pojo.Comment;
import com.stool.minivideo.pojo.vo.CommentVO;
import com.stool.minivideo.utils.MyMapper;

import java.util.List;

public interface CommentMapperCustom extends MyMapper<Comment> {
    public List<CommentVO> queryComments(String videoId);
}