package com.stool.minivideo.mapper;

import com.stool.minivideo.pojo.SearchRecord;
import com.stool.minivideo.utils.MyMapper;

import java.util.List;

public interface SearchRecordMapper extends MyMapper<SearchRecord> {

    public List<String> getHotwords();
}