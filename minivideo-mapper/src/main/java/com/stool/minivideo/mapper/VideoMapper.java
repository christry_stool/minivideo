package com.stool.minivideo.mapper;

import com.stool.minivideo.pojo.Video;
import com.stool.minivideo.utils.MyMapper;

public interface VideoMapper extends MyMapper<Video> {
}