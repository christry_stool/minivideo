package com.stool.minivideo.enums;

import java.util.Arrays;

public enum BGMOperatorTypeEnum {
    ADD("1", "添加bgm"),
    DELETE("2", "删除bgm");

    public final String type;
    public final String value;

    BGMOperatorTypeEnum(String type, String value) {
        this.type = type;
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public String getValue() {
        return value;
    }

    public static String getValueByKey(String key) {
        return  Arrays.stream(BGMOperatorTypeEnum.values())
                .filter(bgmOperatorTypeEnum -> bgmOperatorTypeEnum.getType().equals(key))
                .findAny()
                .map(BGMOperatorTypeEnum::getValue)
                .orElse(null);

//        for (BGMOperatorTypeEnum type : BGMOperatorTypeEnum.values()) {
//            if (type.getType().equals(key)){
//                return type.value;
//            }
//        }
//        return null;
    }

}
