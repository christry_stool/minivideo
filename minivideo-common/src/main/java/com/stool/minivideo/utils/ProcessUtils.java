package com.stool.minivideo.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

public class ProcessUtils {

    public static void startProcess(List<String> command) throws Exception{
        InputStream errorStream = null;
        InputStreamReader inputStreamReader = null;
        BufferedReader br = null;
        try {
            ProcessBuilder builder = new ProcessBuilder(command);
            Process process = builder.start();

            errorStream = process.getErrorStream();
            inputStreamReader = new InputStreamReader(errorStream);
            br = new BufferedReader(inputStreamReader);

            String line = "";
            while ( (line = br.readLine()) != null ) {

                System.out.println(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("process error");
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
                if (inputStreamReader != null) {
                    inputStreamReader.close();
                }
                if (errorStream != null) {
                    errorStream.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
