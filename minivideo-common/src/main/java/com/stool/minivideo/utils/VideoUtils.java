package com.stool.minivideo.utils;

import java.util.ArrayList;
import java.util.List;

public class VideoUtils {

    public static void fetchVideoCover(String ffmepgPath, String videoInputPath, String coverOutputPath) throws Exception{
        // ffmpeg.exe -ss 00:00:01 -i videoInputPath -vframes 1 coverOutputPath
        List<String> command = new ArrayList<>();
        command.add(ffmepgPath);

        // 指定截取第一秒
        command.add("-ss");
        command.add("00:00:01");

        command.add("-y");
        command.add("-i");
        command.add(videoInputPath);

        command.add("-vframes");
        command.add("1");

        command.add(coverOutputPath);

        ProcessUtils.startProcess(command);
    }

    public static void mergeVideoMp3(String ffmepgPath, String videoInputPath, String mp3InputPath, double seconds, String videoOutputPath) throws Exception{
        // ffmpeg.exe -i videoInputPath -i mp3InputPath -t seconds -y videoOutputPath

        List<String> command = new ArrayList<>();
        command.add(ffmepgPath);

        command.add("-i");
        command.add(videoInputPath);

        command.add("-i");
        command.add(mp3InputPath);

        command.add("-t");
        command.add(String.valueOf(seconds));

        command.add("-y");

        // fix acc 开启ffmpeg的acc encoder，这两个参数只能放在最后才能生效
        command.add("-strict");
        command.add("-2");

        command.add(videoOutputPath);

        ProcessUtils.startProcess(command);
    }
}
