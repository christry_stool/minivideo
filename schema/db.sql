drop table if exists user;
drop table if exists user_fan;
drop table if exists video;
drop table if exists user_like_video;
drop table if exists user_report;
drop table if exists comment;
drop table if exists bgm;
drop table if exists search_record;

create table user
(
  id varchar(64) not null,
  username varchar(20) not null,
  password varchar(64) not null,
  face_image varchar(255) comment '头像路径',
  nickname varchar(20) not null,
  fans_counts int(11) not null,
  follow_counts int(11) not null,
  receive_like_counts int(11) not null,

  primary key (id)
);

create table user_fan
(
  id varchar(64) not null,
  user_id varchar(64) not null,
  fan_id varchar(64) not null,

  primary key (id)
);

create table video
(
  id varchar(64) not null,
  user_id varchar(64) not null,
  audio_id varchar(64),
  video_desc varchar(128),
  video_path varchar(255) not null,
  video_seconds float(6, 2),
  video_width int(6),
  video_height int(6),
  cover_path varchar(255) not null,
  like_counts bigint(20) not null,
  status int(1) not null comment '视频状态：1、发布成功；2、禁止播放，管理员操作',
  create_time datetime not null,

  primary key (id)
);

create table user_like_video
(
  id varchar(64) not null,
  user_id varchar(64) not null,
  videio_id varchar(64) not null,

  primary key (id)
);

create table user_report
(
  id varchar(64) not null,
  deal_user_id varchar(64) not null,
  deal_video_id varchar(64) not null,
  title varchar(128) not null,
  content varchar(255),
  user_id varchar(64) not null,
  create_data datetime not null,

  primary key (id)
);

create table bgm
(
  id varchar(64) not null,
  author varchar(255) not null,
  name varchar(255) not null,
  path varchar(255) not null,

  primary key (id)
);

create table search_record
(
  id varchar(64) not null,
  content varchar(255) not null,

  primary key (id)
);

create table comment
(
  id varchar(64) not null,
  video_id varchar(64) not null,
  from_user_id varchar(64) not null,
  comment text not null,
  create_time datetime not null,

  primary key (id)
);

